filetype off
filetype plugin indent off
set rtp+=~/.vim/bundle/Vundle.vim
set clipboard=unnamed
filetype on
au BufNewFile,BufRead *.rs set filetype=rust
filetype plugin indent on
syntax on
set omnifunc=syntaxcomplete#Complete
call vundle#rc()
Bundle 'chriskempson/tomorrow-theme', {'rtp': 'vim/'}
Bundle 'tpope/vim-pathogen'
Bundle 'gmarik/vundle'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'Lokaltog/vim-powerline'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/syntastic'
Bundle 'nathanaelkane/vim-indent-guides'
Bundle 'kien/ctrlp.vim'
Bundle 'SirVer/ultisnips'
Bundle 'ryanmcilmoyl/ultisnips-cf'
Bundle 'surround.vim'
Bundle 'xolox/vim-session'
Bundle 'xolox/vim-misc'
Bundle 'BufOnly.vim'
Bundle 'sjl/gundo.vim.git'
Bundle 'hoffstein/vim-tsql'
Bundle 'bronson/vim-trailing-whitespace'
Bundle 'jelera/vim-javascript-syntax'
Bundle 'pangloss/vim-javascript'
Bundle 'marijnh/tern_for_vim'
Bundle 'majutsushi/tagbar'
Bundle 'junegunn/vim-easy-align'
Bundle 'sjl/splice.vim'
Bundle 'embear/vim-localvimrc'
Bundle 'cf-utils'
Bundle 'altercation/vim-colors-solarized'
Bundle 'groenewege/vim-less'
Bundle 'wting/rust.vim'
Bundle 'cespare/vim-toml'
color solarized
set background=light
set fdm=indent
set autoindent
set ts=2
set sw=2
set et
set number
set nowrap
set nobackup
set noswapfile
set enc=utf-8
set showmatch
set cc=120
map <S-h> gT
map <S-l> gt
nmap <F2> :NERDTreeToggle<CR>
nmap ; :CtrlPBuffer<CR>
vmap <Enter> <Plug>(EasyAlign)
nmap <Leader>a <Plug>(EaseAlign)
nnoremap <F5> :GundoToggle<CR>
color Tomorrow
let g:gundeeo_width = 80
let g:gundo_preview_height = 30
let g:gundo_preview_bottom = 1
let mapleader=" "
let g:splice_prefix="-"
let g:EasyMotion_leader_key = '<Leader>'
let g:EasyMotion_mapping_t = '_t'
let g:UltiSnipsSnippetDirectories=["UltiSnips", "bundle/ultisnips-cf"]
let g:UltiSnipsExpandTrigger = '<c-j>'
let g:UltiSnipsListSnippets = '<c-l>'
set visualbell
hi link EasyMotionTarget ErrorMsg
hi link EasyMotionShade Comment
hi Normal ctermbg=none
set scrolloff=7 " Changes when VIM starts scrolling file (i.e. cursor two lines from bottom)
"CtrlP settings
let g:ctrlp_working_path_mode = 2
let g:ctrlp_custom_ignore = '\.git$|\.hg$|\.svn$|\.DS_Store$|_sgbak$|node_modules'
let g:ctrlp_dotfiles = 0
let g:ctrlp_match_window_bottom = 0
let g:ctrlp_match_window_reversed = 0
let g:ctrlp_max_depth = 40
let g:ctrlp_max_height = 20
"PowerLine settings
let g:Powerline_symbols = 'fancy'
set laststatus=2
"Syntastic statusline stuff
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
"Syntastic settings
let g:syntastic_auto_jump=1
let g:syntastic_auto_loc_list=1
let g:syntastic_stl_format = '[%E{Err: %fe #%e}%B{ - }%W{Warn: %fw #%w}]'
let g:syntastic_mode_map = {'mode': 'active', 'passive_filetypes': ['html']}
let g:sql_type_default = "sqlserver"
let g:indent_guides_start_level = 1
let g:indent_guides_guide_size = 1
autocmd FileType rust setlocal et sw=4 ts=4
